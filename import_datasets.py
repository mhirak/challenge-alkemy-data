#!/usr/bin/env python3

from datetime import date
import requests
from bs4 import BeautifulSoup
import os


datos_gobierno_url  = 'https://datos.gob.ar/dataset/cultura-mapa-cultural-espacios-culturales/archivo/'

url_list = [ datos_gobierno_url + '/cultura_4207def0-2ff7-41d5-9095-d42ae8207a5d',
             datos_gobierno_url + '/cultura_392ce1a8-ef11-4776-b280-6f1c7fae16ae',
             datos_gobierno_url + '/cultura_01c6c048-dbeb-44e0-8efa-6944f73715d7'  ]

# get dataset_link and category
for url in url_list:
    raw_html = requests.get(url)
    soup = BeautifulSoup(raw_html.text,'lxml')
    # find tag with DESCARGAR text, inside a div of partial-class resource-actions
    dataset_link = soup.select_one('div[class*="resource-actions"]').find("a", string = "DESCARGAR")['href']
    # print(soup.select_one('a[class="btn btn-green btn-block"]')['href'])
    category = dataset_link.split('/')[-1].split('.')[0]
    print("{} link = {}".format(category, dataset_link))
    
    parent_dir = "/data"
    try:
        os.mkdir(os.path.join(parent_dir, category))
        print("Carpeta {} creada".format(category))
    except FileExistsError:
        print("Ya existe el directorio")





